decal SlashDecal1
{
	pic KNIFCUT1
	//translucent 0.85
	shade "00 00 00"
	x-scale 0.5
	y-scale 0.25
	randomflipx
	randomflipy
}

decal SlashDecal2
{
	pic KNIFCUT2
	//translucent 0.85
	shade "00 00 00"
	x-scale 0.5
	y-scale 0.25
	randomflipx
	randomflipy
}

decal SlashDecal3
{
	pic KNIFCUT3
	//translucent 0.85
	shade "00 00 00"
	x-scale 0.5
	y-scale 0.25
	randomflipx
	randomflipy
}

decal SlashDecal4
{
	pic KNIFCUT4
	//translucent 0.85
	shade "00 00 00"
	x-scale 0.5
	y-scale 0.25
	randomflipx
	randomflipy
}

decal SlashDecal5
{
	pic KNIFCUT5
	//translucent 0.85
	shade "00 00 00"
	x-scale 0.5
	y-scale 0.25
	randomflipx
	randomflipy
}

decalgroup SlashDecal
{
	SlashDecal1	1
	SlashDecal2	1
	SlashDecal3	1
	SlashDecal4	1
	SlashDecal5	1
}